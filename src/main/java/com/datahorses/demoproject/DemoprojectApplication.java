package com.datahorses.demoproject;

import com.datahorses.demoproject.loans.Loan;
import com.datahorses.demoproject.loans.services.LoanCheckerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.URL;
import java.util.List;

@SpringBootApplication
public class DemoprojectApplication implements CommandLineRunner {

	@Autowired
	private LoanCheckerService loanCheckerService;

	public static void main(String[] args) {
		SpringApplication.run(DemoprojectApplication.class, args);
	}

	@Override
	public void run(String... args) {
		Long interval = null;
		if (args.length > 0) {
			interval = Long.parseLong(args[0]);
		}
		loanCheckerService.checkForNewLoans(interval);
	}

}
