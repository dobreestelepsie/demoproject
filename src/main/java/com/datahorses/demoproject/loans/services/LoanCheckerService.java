package com.datahorses.demoproject.loans.services;

import com.datahorses.demoproject.loans.Loan;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class LoanCheckerService {

    @Value("${zonky.address}")
    private String zonkyUrl;
    private List<Loan> loans;

    @PostConstruct
    private void init() {
        this.loans = new ArrayList<>();
        printLast20Loans();
    }

    //There would be a problem with this solution if there's more than 20 new loans for given time interval
    //solution for this would be:
    // while (20 new loans) {use pagination to another page}
    public void checkForNewLoans(final Long timeInterval) {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                List<Loan> newLoans = getNewLoans();
                if (newLoans.size() > 0) {
                    System.out.println("New loans:");
                    loans.addAll(newLoans);
                    newLoans.forEach(System.out::println);
                } else {
                    System.out.println("There are no new loans at this time");
                }
            }
        };
        Timer timer = new Timer();
        timer.schedule(task, 0, timeInterval == null ? 300000l : timeInterval);
    }

    private List<Loan> getNewLoans() {
        List<Loan> actualLoans = getLoans();
        List<Loan> diff = this.loans.stream()
                .filter(i -> !actualLoans.contains(i))
                .collect (Collectors.toList());
        return diff;
    }

    private void printLast20Loans() {
        List<Loan> loans = getLoans();
        System.out.println("Poslednych 20 poziciek:");
        loans.forEach(System.out::println);
    }

    private List<Loan> getLoans() {
        final String uri = zonkyUrl+"?fields=id,url,name,amount,rating";
        final ObjectMapper mapper = new ObjectMapper();
        try {
            return  mapper.readValue(new URL(uri), new TypeReference<List<Loan>>(){});
        } catch (IOException e) {
            System.err.println("Sorry, we were unable to fetch loans.");
        } catch (Exception e) {
            System.err.println("Sorry, we were unable to fetch loans.");
        }
        return new ArrayList<>();
    }
}
