package com.datahorses.demoproject.loans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

//we ignore properties that we don't care about
@JsonIgnoreProperties(ignoreUnknown = true)
public class Loan {

    private Long id;
    private String Url;
    private String Name;
    private String rating;
    private Double amount;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Loan{" +
                "id=" + id +
                ", Url='" + Url + '\'' +
                ", Name='" + Name + '\'' +
                ", rating='" + rating + '\'' +
                ", amount=" + amount +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Loan) {
            return ((Loan) obj).getId().equals(getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
