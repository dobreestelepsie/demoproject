HOW TO RUN PROGRAM

ensure your environment variable JAVA_HOME is pointing to java 11 jdk
for example: export JAVA_HOME=/usr/lib/jvm/java-11-openjdk/
(java 11 may not be needed, but it was tested on)

to run program with refresh time argument:
example with refresh rate 5s:
mvn spring-boot:run -Dspring-boot.run.arguments="5000"

run program without arguments:
mvn spring-boot:run
